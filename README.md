# Conduit.API

API written in .NET Core which serves articles and is able to group them by tags.

## Getting started
1.  Requirements
*  [.NET Core SDK](https://dotnet.microsoft.com/download/dotnet-core/2.2)
*  [SQL Server Express](https://www.microsoft.com/en-us/sql-server/sql-server-editions-express)
*  [.NET Core IIS Hosting bundle - OPTIONAL](https://www.microsoft.com/net/permalink/dotnetcore-current-windows-runtime-bundle-installer)

2.  Build
*  `dotnet build Conduit.sln -c Release`

3.  Run
*  `cd src\Conduit\bin\Release\netcoreapp2.2`
*  `dotnet Conduit.dll`

4.  Test
*  Navigate to http://localhost:5000/swagger to see if the app is working and is serving the Swagger documentation for the API.